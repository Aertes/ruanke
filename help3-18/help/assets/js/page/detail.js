/* ------------------------------------------------------------------------------
*
*  # Detail
*
*  Specific JS code additions for detail.jsp page
*
*  Version: 1.0
*  Latest update: February. 24, 2017
*
* ---------------------------------------------------------------------------- */
$(function() {

	     var detailtime;
    	$.ajax({
	        type : 'POST', 
	        url : 'detail.action',
	        dataType:"json",
	        data : {
	            url : 'detail'
	        },
	        beforeSend:function(XMLHttpRequest){
	        	detailtime = layer.load(1, {
	        		  shade: [0.1,'#fff']
	        		});
	       	},
	        success : function (indicator) {
	        	layer.close(detailtime);
	        	var tempvar=["","","","","","","","","","",""];
	        	var cospanVar="";
	        	var tempint=0;
	        	var lastmodule=0;
	     		   for(var i =0;i<indicator.data.length;i++)
	     			   {
	     			      var tempvar22="";
	     			     if(indicator.data[i].sdataState==1)
    			    	  {
	     			    	tempvar22="<span class=\"label label-danger\">数据审核中</span>";
    			    	  }
    			      
	     			      if(indicator.data[i].submodule!=cospanVar)
	     			    	  {
	     			    	    cospanVar=indicator.data[i].submodule;
	     			    	    if(tempint==0){}else 
	     			    	    	{
	     			    	    	  tempvar[lastmodule]=tempvar[lastmodule].replace("cospanvar",tempint);
	     			    	    	}
	     			    	   if(indicator.data[i].sdataEditable==1) tempvar[parseInt(indicator.data[i].smodule)]+="<tr><td class=\"tdno1\" rowspan=\"cospanvar\">"+indicator.data[i].submodule+"</td><td class=\"tdno2\" title=\"数据定义："+indicator.data3[i].indicatorDefinition+"\n数据来源："+indicator.data3[i].indicatorSource+"\">"+indicator.data[i].sindicatorWebid+". "+indicator.data[i].sindicator+" <i class=\"icon-question3\"></i></td><td class=\"tdno3\"><span class=\"data-background\">"+indicator.data[i].sindicatorData+"</span></td><td class=\"tdno4\">"+indicator.data[i].sindicatorYear+"</td><td class=\"tdno5\">"+indicator.data[i].sidndicatroDescription+"</td><td class=\"tdno6\"><ul class=\"icons-list\"><li><label title=\"修改该数据\" id=\"idstate-"+indicator.data[i].sindicatorId+"\" class=\"idoperation\"><i class=\"icon-pencil7\"></i></label></li><li class=\"datahistory\" id=\"datahistory-"+indicator.data[i].sindicatorId+"\"><label class=\"datahistorys\" title=\"查看修改记录与状态\"><i class=\"icon-history\"></i></label></li></ul></td><td class=\"tdno7\"><ul class=\"icons-list\"><li id=\"datacheckstate-"+indicator.data[i].sindicatorId+"\">"+tempvar22+"</li></ul></td></tr>";
	     			    	   else  {
	     			    		  if(indicator.data[i].sprovideDetail==1) {
	     			    			 if(indicator.data[i].smodule==9) tempvar[parseInt(indicator.data[i].smodule)]+="<tr><td class=\"tdno1\" rowspan=\"cospanvar\">"+indicator.data[i].submodule+"</td><td class=\"tdno2\" title=\"数据定义："+indicator.data3[i].indicatorDefinition+"\n数据来源："+indicator.data3[i].indicatorSource+"\">"+indicator.data[i].sindicatorWebid+". "+indicator.data[i].sindicator+" <i class=\"icon-question3\"></i></td><td class=\"tdno3\">"+indicator.data[i].sindicatorData+"</td><td class=\"tdno4\">"+indicator.data[i].sindicatorYear+"</td><td class=\"tdno5\">"+indicator.data[i].sidndicatroDescription+"</td><td class=\"tdno6\" id=\"idstate-"+indicator.data[i].sindicatorId+"\"><ul class=\"icons-list\"><li><label class=\"data-background dataDetailprovide2\" id=\"getdata-"+indicator.data[i].sindicatorId+"-All"+"-"+indicator.data[i].sindicator+"-"+indicator.data[i].sindicatorYear.replace("-","_").replace("截至","_")+"-temp"+"\" title=\"查看"+indicator.data[i].sindicator.replace("（","(").replace("）",")").replace(/\([^\)]*\)/g,"")+"明细\"><i class=\" icon-list-numbered\"></i></label></li></ul></td><td class=\"tdno7\"></td></tr>";
	     			    			 else  tempvar[parseInt(indicator.data[i].smodule)]+="<tr><td class=\"tdno1\" rowspan=\"cospanvar\">"+indicator.data[i].submodule+"</td><td class=\"tdno2\" title=\"数据定义："+indicator.data3[i].indicatorDefinition+"\n数据来源："+indicator.data3[i].indicatorSource+"\">"+indicator.data[i].sindicatorWebid+". "+indicator.data[i].sindicator+" <i class=\"icon-question3\"></i></td><td class=\"tdno3\">"+indicator.data[i].sindicatorData+"</td><td class=\"tdno4\">"+indicator.data[i].sindicatorYear+"</td><td class=\"tdno5\">"+indicator.data[i].sidndicatroDescription+"</td><td class=\"tdno6\" id=\"idstate-"+indicator.data[i].sindicatorId+"\"><ul class=\"icons-list\"><li><label class=\"data-background dataDetailprovide\" id=\"getdata-"+indicator.data[i].sindicatorId+"-All"+"-temp"+"-"+indicator.data[i].sindicatorYear.replace("-","_").replace("现职","_2015")+"-temp"+"\" title=\"查看"+indicator.data[i].sindicator.replace("（","(").replace("）",")").replace(/\([^\)]*\)/g,"")+"明细\"><i class=\" icon-list-numbered\"></i></label></li><li class=\"datahistory\" id=\"datahistory-"+indicator.data[i].sindicatorId+"\"><label class=\"datahistorys\" title=\"查看修改记录与状态\"><i class=\"icon-history\"></i></label></li></ul></td><td class=\"tdno7\"><ul class=\"icons-list\"><li id=\"datacheckstate-"+indicator.data[i].sindicatorId+"\">"+tempvar22+"</li></ul></td></tr>";
	     			    		  }
	     			    		  else tempvar[parseInt(indicator.data[i].smodule)]+="<tr><td class=\"tdno1\" rowspan=\"cospanvar\">"+indicator.data[i].submodule+"</td><td class=\"tdno2\" title=\"数据定义："+indicator.data3[i].indicatorDefinition+"\n数据来源："+indicator.data3[i].indicatorSource+"\">"+indicator.data[i].sindicatorWebid+". "+indicator.data[i].sindicator+" <i class=\"icon-question3\"></i></td><td class=\"tdno3\"><span class=\"data-background\">"+indicator.data[i].sindicatorData+"</span></td><td class=\"tdno4\">"+indicator.data[i].sindicatorYear+"</td><td class=\"tdno5\">"+indicator.data[i].sidndicatroDescription+"</td><td class=\"tdno6\"></td><td class=\"tdno7\"></td></tr>";
	     			    	   }
	     			    	   tempint=1;
	     			    	   lastmodule=parseInt(indicator.data[i].smodule);
	     			    	  }
	     			      else{
	     			    	    if(indicator.data[i].sdataEditable==1) tempvar[parseInt(indicator.data[i].smodule)]+="<tr><td class=\"tdno2\" title=\"数据定义："+indicator.data3[i].indicatorDefinition+"\n数据来源："+indicator.data3[i].indicatorSource+"\">"+indicator.data[i].sindicatorWebid+". "+indicator.data[i].sindicator+" <i class=\"icon-question3\"></i></td><td class=\"tdno3\"><span class=\"data-background\">"+indicator.data[i].sindicatorData+"</span></td><td class=\"tdno4\">"+indicator.data[i].sindicatorYear+"</td><td class=\"tdno5\">"+indicator.data[i].sidndicatroDescription+"</td><td class=\"tdno6\"><ul class=\"icons-list\"><li><label title=\"修改该数据\" id=\"idstate-"+indicator.data[i].sindicatorId+"\" class=\"idoperation\"><i class=\"icon-pencil7\"></i></label></li><li class=\"datahistory\" id=\"datahistory-"+indicator.data[i].sindicatorId+"\"><label class=\"datahistorys\" title=\"查看修改记录与状态\"><i class=\"icon-history\"></i></label></li></ul></td><td class=\"tdno7\"><ul class=\"icons-list\"><li id=\"datacheckstate-"+indicator.data[i].sindicatorId+"\">"+tempvar22+"</li></ul></td></tr>";
	     			    	    else  {
	     			    	    	if(indicator.data[i].sprovideDetail==1) {
	     			    	    		if(indicator.data[i].smodule==9) tempvar[parseInt(indicator.data[i].smodule)]+="<tr><td class=\"tdno2\" title=\"数据定义："+indicator.data3[i].indicatorDefinition+"\n数据来源："+indicator.data3[i].indicatorSource+"\">"+indicator.data[i].sindicatorWebid+". "+indicator.data[i].sindicator+" <i class=\"icon-question3\"></i></td><td class=\"tdno3\">"+indicator.data[i].sindicatorData+"</td><td class=\"tdno4\">"+indicator.data[i].sindicatorYear+"</td><td class=\"tdno5\">"+indicator.data[i].sidndicatroDescription+"</td><td class=\"tdno6\" id=\"idstate-"+indicator.data[i].sindicatorId+"\"><ul class=\"icons-list\"><li><label class=\"data-background dataDetailprovide2\" id=\"getdata-"+indicator.data[i].sindicatorId+"-All"+"-"+indicator.data[i].sindicator+"-"+indicator.data[i].sindicatorYear.replace("-","_").replace("截至","_")+"-temp"+"\" title=\"查看"+indicator.data[i].sindicator.replace("（","(").replace("）",")").replace(/\([^\)]*\)/g,"")+"明细\"><i class=\" icon-list-numbered\"></i></label></li></ul></td><td class=\"tdno7\"></td></tr>";
	     			    	    		else  tempvar[parseInt(indicator.data[i].smodule)]+="<tr><td class=\"tdno2\" title=\"数据定义："+indicator.data3[i].indicatorDefinition+"\n数据来源："+indicator.data3[i].indicatorSource+"\">"+indicator.data[i].sindicatorWebid+". "+indicator.data[i].sindicator+" <i class=\"icon-question3\"></i></td><td class=\"tdno3\">"+indicator.data[i].sindicatorData+"</td><td class=\"tdno4\">"+indicator.data[i].sindicatorYear+"</td><td class=\"tdno5\">"+indicator.data[i].sidndicatroDescription+"</td><td class=\"tdno6\" id=\"idstate-"+indicator.data[i].sindicatorId+"\"><ul class=\"icons-list\"><li><label class=\"data-background dataDetailprovide\" id=\"getdata-"+indicator.data[i].sindicatorId+"-All"+"-temp"+"-"+indicator.data[i].sindicatorYear.replace("-","_").replace("现职","_2015")+"-temp"+"\" title=\"查看"+indicator.data[i].sindicator.replace("（","(").replace("）",")").replace(/\([^\)]*\)/g,"")+"明细\"><i class=\" icon-list-numbered\"></i></label></li><li class=\"datahistory\" id=\"datahistory-"+indicator.data[i].sindicatorId+"\"><label class=\"datahistorys\" title=\"查看修改记录与状态\"><i class=\"icon-history\"></i></label></li></ul></td><td class=\"tdno7\"><ul class=\"icons-list\"><li id=\"datacheckstate-"+indicator.data[i].sindicatorId+"\">"+tempvar22+"</li></ul></td></tr>";
	     			    	    	}
	     			    	    	else tempvar[parseInt(indicator.data[i].smodule)]+="<tr><td class=\"tdno2\" title=\"数据定义："+indicator.data3[i].indicatorDefinition+"\n数据来源："+indicator.data3[i].indicatorSource+"\">"+indicator.data[i].sindicatorWebid+". "+indicator.data[i].sindicator+" <i class=\"icon-question3\"></i></td><td class=\"tdno3\"><span class=\"data-background\">"+indicator.data[i].sindicatorData+"</span></td><td class=\"tdno4\">"+indicator.data[i].sindicatorYear+"</td><td class=\"tdno5\">"+indicator.data[i].sidndicatroDescription+"</td><td class=\"tdno6\"></td><td class=\"tdno7\"></td></tr>";
	     			    	    }
	     			    	    tempint++;
	     			    	  	lastmodule=parseInt(indicator.data[i].smodule);
	     			      }
	     			      
	     			      
	     			   }
	     		   tempvar[10]=tempvar[10].replace("cospanvar",tempint);
	     		   for(var j=1;j<11;j++)
	     			   {
	     			   		$("#module"+j+"-nestedtable").html(tempvar[j]);
	     			   }
	     		   
	        }
	    });
    	
    	
    	$(document).on('mouseover', '.dataDetailprovide', function() {
        	$('.dataDetailprovide').css('cursor', 'pointer');
        });
    	$(document).on('mouseover', '.dataDetailprovide2', function() {
        	$('.dataDetailprovide2').css('cursor', 'pointer');
        });
    	$(document).on('mouseover', '.idoperation', function() {
        	$('.idoperation').css('cursor', 'pointer');
        });
    	$(document).on('mouseover', '.datahistorys', function() {
        	$('.datahistorys').css('cursor', 'pointer');
        });
    	var tanchukuanggaodu="700px";
    	if(parseInt(window.screen.width)<=1400) tanchukuanggaodu="500px";
    	$(document).on('click', '.dataDetailprovide', function() {
    		var userId=this.id;
        	layer.open({
    	        type: 2,
    	        title: ""+this.title+"",
    	        fix:true,
    	        maxmin: true,
    	        shadeClose: false, 
    	        area : ['1000px' , tanchukuanggaodu],
    	        content: 'indicatordetail.jsp?param='+userId
    	    });
        });
    	
    	$(document).on('click', '.dataDetailprovide2', function() {
    		var userId=this.id;
        	layer.open({
    	        type: 2,
    	        title: ""+this.title+"",
    	        fix:true,
    	        maxmin: true,
    	        shadeClose: false, 
    	        area : ['1000px' , tanchukuanggaodu],
    	        content: 'indicatordetail2.jsp?param='+userId
    	    });
        });
    	
    	
    	$(document).on('click', '.datahistory', function() {
    		var userId=this.id;
    		$.ajax({
    	        type : 'POST', 
    	        url : 'addperson.action',
    	        dataType:"json",
    	        data : {
    	            url1 : 'checkhistoryrecord',
    	            url2 : userId
    	        },
    	        success : function (indicator) {
                    if(indicator.data==1)
                    	{
                    	layer.open({
                	        type: 2,
                	        title: "数据修改记录",
                	        fix:true,
                	        maxmin: true,
                	        shadeClose: false, 
                	        area : ['900px' , tanchukuanggaodu],
                	        content: 'viewmodifyhistory.jsp?param='+userId
                	    });
                    	}
                    else{
                    	layer.msg('该指标没有修改记录！', function(){});
      				    return false;

                    }
    	     		   
    	        }
    	    });
        });
    	
    	
    	
    	
    	
    	
    	
    	
    	$(document).on('click', '.idoperation', function() {
    		var userId=this.id;
    		$.ajax({
    	        type : 'POST', 
    	        url : 'addperson.action',
    	        dataType:"json",
    	        data : {
    	            url1 : 'checkstate',
    	            url2 : userId
    	        },
    	        success : function (indicator) {
                    if(indicator.data==1)
                    	{
                    	  layer.msg('该数据在审核中，请等待审核完成！', function(){});
        				  return false;
                    	}
                    else{

                		layer.open({
                	        type: 2,
                	        title: "修改数据",
                	        fix:true,
                	        maxmin: true,
                	        shadeClose: false, 
                	        area : ['500px' , '550px'],
                	        content: 'changevalue.jsp?param='+userId
                	    });
                    }
    	     		   
    	        }
    	    });
    		
    		
    		
        });
    	
    	

        //空白处可点击
        $(document).on('click', '.heading-control', function() {
        	var panelId=this.id.replace("heading-control","collapsible-control-right-group");
        	$("#"+panelId).collapse('toggle');
        });
        $(document).on('mouseover', '.heading-control', function() {
        	$('#'+this.id).css('cursor', 'pointer');
        });


		




    
});
